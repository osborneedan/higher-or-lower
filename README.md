# Higher or Lower

Just a basic higher or lower number game written in Python.

## Screenshot

![Screenshot of the game on Arch Linux KDE](https://i.ibb.co/YdQmqB1/Screenshot-20220122-112041.png)

## Installation

### Linux and BSD

1. If you haven't already, install Python:

   For Debian, Ubuntu and derivatives: `sudo apt install python`

   For Fedora: `sudo dnf install python3.11`

   For CentOS, RHEL and others: `sudo dnf install python3`

   For openSUSE: `sudo zypper in python3`

   For Arch: `sudo pacman -S python3`

   For Gentoo: Already installed

   For FreeBSD: `pkg install python3`

   For OpenBSD: `pkg_add python` and select the latest version when prompted

2. Either clone the repository with `git clone https://gitlab.com/osborneedan/higher-or-lower.git` or download `higherorlower`
3. Make the file executable with `chmod +x /path/to/file`
4. Optionally move the file to `/bin` or `/usr/bin`

### macOS

1. Visit ![Python.org](https://www.python.org/downloads/macos/) and install the latest version of IDLE
2. Either clone the repository with `git clone https://gitlab.com/osborneedan/higher-or-lower.git` or download `higherorlower`
3. Rename `higherorlower` to `higherorlower.py`
4. Open and run `higherorlower.py` in IDLE

### Windows, WineHQ and ReactOS

1. Visit ![Python.org](https://www.python.org/downloads/windows/) and install the latest version of IDLE
2. Either clone the repository with `git clone https://gitlab.com/osborneedan/higher-or-lower.git` or download `higherorlower`
3. Rename `higherorlower` to `higherorlower.py`
4. Open and run `higherorlower.py` in IDLE
